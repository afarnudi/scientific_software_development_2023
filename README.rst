Scientific Software Development 2023
====================================

In this repository, I will include all relevant material. It will be updated frequently during the course.

Course requirements
-------------------

* Python 3
* Visual Studio Code (or a similar IDE)
* git (Windows users: https://gitforwindows.org)
* Basic Shell/Command line skills (If in dought, look at the first 3 sections of https://swcarpentry.github.io/shell-novice/)

Timetable
---------

This is a rough plan, but we may switch up some items at short notice.
Join the `Discord channel <https://discord.gg/N6hfqG9r>`_


Day 1 (13 Sept)
...................

* 13:30 - 14:00   Overview and introduction to the course - `Course overview <day01/2023-ssd-overview.pdf>`_
* 14:00 - 14:20   Python intro - `slides <day01/2023-ssd-python-intro.pdf>`_
* 14:20 - 14:35   Git and IDE setup
* 14:35 - 15:20   Version control with Git part 1 - `part 1: individual use <day01/2023-ssd-Git-part_1.pdf>`_
* 15:20 - 15:40   Break
* 15:40 - 16:15   Practice git: https://projecteuler.net/archives A(1, 2, 3), B(14, 17 (use dict), 57), C(79 (file input), 102 (handle 2D points))
* 16:15 - 16:40   `Clean code <day01/2023-ssd-Clean-code.pdf>`_ and `docstrings <day01/2023-ssd-docstrings.pdf>`_ 
* 16:40 - 17:30   Code review


Day 2 (20 Sept)
...............
* 13:30 - 13:40   Day 1 Review + Q&A
* 13:40 - 14:30   Git `part 2: branches / merging <day02/2023-ssd-Git-part_2.pdf>`_
* 14:30 - 15:30   Exercise (`land/water <https://ictp.grelli.org/sd2021/landcover/>`_)
* 15:30 - 15:45   Break
* 15:45 - 16:15   Code review (code readability, codestyle/PEP8, codedocstyle)
* 16:15 - 17:00   Argument handling
* 17:00 - 17:30   Hands-on Argpars

Day 3 (27 Sept)
...............
* 13:30 - 13:40   Day 2 Review + Q&A + talk about moving the last two sessions.
* 13:40 - 14:30   `Programming paradigms <day03/2023-ssd-programming_paradigms.pdf>`_ and `OO-Design <day03/2023-ssd-OO-Design_David.pdf>`_
* 14:30 - 15:00   Train station exercise
* 15:00 - 15:20   Train station playoff
* 15:20 - 15:35   Break
* 15:35 - 16:30   Train station implementation in Python (`data <day03/trains>`_) 
* 16:30 - 17:00   `Unit testing <day03/2023-ssd-UT.pdf>`_ and test suits (`examples <day03/UT_examples>`_)
* 17:00 - 17:30   `Exercises <day03/UT_exercises>`_: unit testing and code review


Day 4 (4 Oct)
...............
* 13:30 - 13:40   Day 3 Review + Q&A
* 13:40 - 14:10   `licensing <day04/2023-ssd-Licensing.pdf>`_
* 14:10 - 14:50   Git part 3
* 14:50 - 15:30   Project setup + README + License
* 15:30 - 15:50   Break   
* 15:50 - 16:20   Python environments (hands-on demo)
* 16:20 - 17:10   `Docker <day04/2023-ssd-Docker.pdf>`_ + development (`example <day04/docker_example>`_)
* 17:10 - 17:30   Code review



Day 5 (25 Oct)
.............
* 13:30 - 13:40   Day 4 Review + Q&A
* 13:40 - 15:00   Code review
* 15:00 - 15:20   Break
* 15:20 - 16:00   Setup CI
* 16:00 - 16:40   `Packaging <day05/2023-ssd-Packaging.pdf>`_
* 16:40 - 17:30   Hands-on CI and packaging



Day 6 (22 Nov)
..............
* 13:30 - 13:40   Day 5 Review + Q&A
* 13:40 - 14:10   Lists vs Numpy arrays (`code <day06/list_arr_timing.py>`_)
* 14:10 - 14:45   Python `virtual environments tutorial <https://realpython.com/python-virtual-environments-a-primer/>`_
* 14:45 - 15:00   `pyproject.toml <day06/pyproject.toml>`_
* 15:00 - 15:20   Break
* 15:20 - 17:00   Hands-on: setup project workflow (example `.gitlab-ci.yml <day06/.gitlab-ci.yml>`_) 
* 17:00 - 17:15   `Sphinx <day06/Sphinx.pdf>`_ + `examples <day06/SphinxExample>`_
* 17:15 - 17:45   `Makefile <day06/Makefile.pdf>`_ + `examples <day06/Makefile_examples>`_ + `exercises <day06/Makefile_exercises>`_


Technical session 1 (13 Dec)
............................
**What do I need to do before?**

1. Prepare a short presentation of what you will be doing for your project (min: 5 minutes, max: 15 minutes).
2. Draw a road map of what you need to do to finish the project. (free to choose the medium: in your notes, in slides, etc)
3. Think about/Decide the programming paradigm you will adopt. (modular, OO, functional, etc)
4. What programming languages will you use?
5. Give me a big picture of your software by answering the following questions:

   * What is the main product of your software? (the key service your software will provide)
   * Who will use your software? (students of a particular major, your group/lab/colleagues, anyone on planet earth, ...)
   * What is the user input? (what are the user choices/parameters, how many parameters must a user set, what sort  of questions they should answer)
   * What will be delivered by the software? (a graph, a website, a video, data, etc)
   * How will the user interact with the software? (the terminal, interactive UI, library, API, configuration text file input, etc)
   * How will a user learn to use your software? (Documentation on your website, tutorials, examples, videos, etc)

**What we will do:**

* Discuss your answers to the questions above. (If you did not know how to answer some of the questions, we will find one together)
* Come up with a prototype for your software. (What are the most simplistic and stupid products you can build? How you will improve them later.)
* Design a workflow pipeline for your project. (Make sure your ISSUES, unit tests, CI, packaging, Sphinx webpage, etc are all working)


Technical session 2 (24 Jan 2024)
.................................
Wrap up loose ends and prepare all projects for delivery.





