import numpy as np
import matplotlib as mpl


class Lamp:
    def __init__(self, on=0, th=0, phi=0):
        self._is_on = on
        self._theta = th
        self._phi = phi
        self._preset_theta = np.pi
        self._preset_phi = np.pi/2
        
    def lamp_on_state(self):
        if self._is_on:
            return 1
        else:
            return 0

    def turn_off(self):
        self._is_on = 0
        self._theta = 0
        self._phi = np.pi/2


    def turn_on(self):
        self._is_on = 1
        self._theta = self._preset_theta
        self._phi = self._preset_phi

    def __str__(self):
        if self._is_on:
            return f"The lamp is on!"
        else:
            return f"The lamp is off!"
def main():
    lamp_1 = Lamp()
    lamp_2 = Lamp()

    lamp_1.turn_off()


    string = "ali"
    string = string.upper
    
    print([1,2,3,4,       5])
    print(lamp_1)

if __name__ == "__main__":
    main()
