To reproduce the website in the Sphinx slides, 

1) follow the slides to install sphinx.
2) run 
```
sphinx-quickstart
```
in this directory. Tell Sphinx to seporate the source and build directories.
3) copy the content of the "files" directory (including the "files/source" subdirectory) and past them in your woking directory (where you see the "source" and the "build" directory). Note replace the existing files with the versions you find in the "files" directory.
4) run 
``` 
make html
```
in the same directory you executed step 1).
5) open "index.html" located in the build directory.
