#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
An example for the application of the 'argparse' library.
It is a showcase of how one can use a positional argument (num) and
the store_true action (print) to interact with the user. 
"""

from functions import fibonacci

import argparse


def main():
    parser = argparse.ArgumentParser(
        "Fibonacci Calculator", description="Calculate Fibonacci numbers"
    )
    parser.add_argument("num", help="Print the n'th Fibonacci number.", type=int)
    parser.add_argument(
        "-p",
        "--print",
        help="Print the Fibonacci sequence up to the n'th number",
        action="store_true",
    )
    args = parser.parse_args()
    if args.print == True:
        for i in range(2, args.num + 1):
            print(fibonacci(i), end=" ")
    else:
        print(f"Fib({args.num}) = {fibonacci(args.num)}")


if __name__ == "__main__":
    main()
