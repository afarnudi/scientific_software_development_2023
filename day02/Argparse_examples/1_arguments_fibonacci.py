#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
An example to take one number from the terminal, inturpret it as an integer, 
and pass it a s input to the fibonacci number generator.
"""

import sys
from functions import fibonacci


def main():
    print(fibonacci(int(sys.argv[1])))


if __name__ == "__main__":
    main()
