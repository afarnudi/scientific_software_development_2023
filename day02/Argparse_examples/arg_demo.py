"""
An example that uses basic features of the argparse library to get two inputs from the user and 
display the sum of the numbers.
"""
import argparse


def create_parser():
    """
    Create a parser using the argparse library with two positional arguments (integers).

    Returns:
        argparse.ArgumentParser:
            Generated argument parser.
    """
    parser = argparse.ArgumentParser(
        "SumNum",
        description="Calculate the sum of two numbers",
    )
    parser.add_argument(
        "num_1",
        help="One of the two numbers to use in the sum.",
        type=float,
    )
    parser.add_argument(
        "num_2",
        help="Another number to use in the sum.",
        type=float,
    )
    return parser


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()
    num_sum = args.num_1 + args.num_2
    print(f"\n\nHello!!!\nThe sum is {num_sum}\n\n")
