#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
An example to show how terminal arguments are accessed inside Python through the 'sys' library.
"""
import sys

print(f"\nNumber of arguments:, {len(sys.argv)}")
print("Argument list:")

for arg in sys.argv:
    print(arg)
print("\n")
