def fibonacci(n):
    """
    Generate the n'th number in the Fibonacci sequence.

    Args:
        n (int): The position of the number in the Fibonacci sequence.

    Returns:
        int: The n'th number in the Fibonacci sequence.
    """
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a


if __name__ == "__main__":
    print("Hello. I am fibonacci.")
